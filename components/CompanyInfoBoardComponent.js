import CompanyMapComponent from "./CompanyMapComponent";
import styles from "../styles/CompanyInfoBoardComponent.module.css";

export default function CompanyInfoBoardComponent({ company }) {
    const makeListElement = (description, content) => <li className={styles.listElement}>
        <span className={styles.content}>{content}</span>
        <br />
        <span className={styles.description}>{description}</span>
    </li>;

    return <div className={styles.container}>
        <ul className={styles.list}>
            {makeListElement('Nom', company.name)}
            {makeListElement('Secteur', company.category)}
            {makeListElement('Adresse', company.address)}
            {makeListElement('SIREN', company.SIREN)}
        </ul>
        <CompanyMapComponent address={company.address} />
    </div>;
}