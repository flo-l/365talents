import styles from '../styles/CompanyMapComponent.module.css';

export default function CompanyMapComponent( { address } ) {
    const api_key = process.env.NEXT_PUBLIC_GOOGLE_MAPS_API_KEY;
    return <div className={styles.container}>
        <iframe
            className={styles.iframe}
            frameBorder="0"
            referrerPolicy="no-referrer-when-downgrade"
            src={`https://www.google.com/maps/embed/v1/place?key=${api_key}&q=${encodeURIComponent(address)}&language=fr`}
            allowFullScreen={true}
            allow="encrypted-media"
        />
    </div>
}