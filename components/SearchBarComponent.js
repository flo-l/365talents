import {useState} from "react";
import styles from '../styles/SearchBarComponent.module.css';

export default function SearchBarComponent({ searchButtonClicked }) {
    const [value, setValue] = useState('');

    const handleSubmit = (event) => {
        event.preventDefault();
        searchButtonClicked(value);
    };

    return <form className={styles.form} onSubmit={handleSubmit}>
        <input
            className={styles.input}
            type="text"
            id="header-search"
            autoFocus={true}
            placeholder="365Talents"
            value={value}
            onChange={(e) => setValue(e.target.value) }
        />
        <button className={styles.button} type="submit">Rechercher</button>
    </form>
}