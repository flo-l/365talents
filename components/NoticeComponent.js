import styles from "../styles/NoticeComponent.module.css";

export default function NoticeComponent({ children }) {
    return <p className={styles.p}>{ children }</p>;
}