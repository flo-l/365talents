// Next.js API route support: https://nextjs.org/docs/api-routes/introduction

import axios from "axios";
import cheerio from "cheerio";

const baseUrl = 'https://www.societe.com';

const headers = {
  "User-Agent": 'Mozilla/5.0 (X11; Linux x86_64; rv:102.0) Gecko/20100101 Firefox/102.0',
};

const axios_options = {
  responseType: 'arraybuffer',
  responseEncoding: 'binary',
  headers,
};

const cheerio_options = {
  decodeEntities: true,
}

let encoding = 'ISO-8859-1';
//encoding = 'utf-8';
const decoder = new TextDecoder(encoding);

const fetch = async (url) => {
  const res = await axios.get(url, axios_options);
  return decoder.decode(res.data);
}

const search = async (query) => {
  const url = (query) => `${baseUrl}/cgi-bin/search?champs=${query}`;

  const html = await fetch(url(query));
  const $ = cheerio.load(html, cheerio_options);
  const results = $('div#result_rs_societe>div');

  if (results.length === 0) {
    return null;
  }

  const first = results.first();
  return $('div>a', first).first().attr('href');
}

const scrap = async (company_path) => {
  const url = `${baseUrl}/${company_path}`;

  const html = await fetch(url);
  const $ = cheerio.load(html, cheerio_options);

  const name = $('h1#identite_deno').first().text().trim();

  const extract = (what) =>
      $('table#rensjur>tbody>tr>td').filter(function() {
        return $(this).text().trim() === what;
      }).first().next().text().trim();

  return {
    name,
    address: extract('Adresse postale'),
    SIREN: extract('Numéro SIREN').split('\n')[0],
    category: extract('Catégorie'),
  };
}

export default async function handler(req, res) {
  const make_err = (body={ error: 'api_error' }, status=500) => {
    res.status(status).json(body);
  };

  try {
    const query = req.body && req.body['query'];
    if (!query) {
      return make_err();
    }

    const path = await search(query);
    if (!path) {
      res.status(200).json(null);
    }

    const data = await scrap(path);
    res.status(200).json(data);
  } catch (err) {
    console.error("error in API", err);
    return make_err();
  }
}
