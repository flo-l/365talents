import {useEffect, useState} from "react";
import Head from 'next/head'
import SearchBarComponent from "../components/SearchBarComponent";
import styles from '../styles/Home.module.css'
import CompanyInfoBoardComponent from "../components/CompanyInfoBoardComponent";
import NoticeComponent from "../components/NoticeComponent";

export default function Home() {
    const [query, setQuery] = useState(null);
    const [company, setCompany] = useState(undefined);
    const [error, setError] = useState(null);
    const [loading, setLoading] = useState(false);
    const [shouldRefetch, setShouldRefetch] = useState(false);

    useEffect(() => {
        const f = async () => {
            if (query) {
                setLoading(true);
                setError(false);
                const request = new Request('/api/search', {
                    method: 'POST',
                    body: JSON.stringify({ query }),
                    headers: {
                        'Content-Type': 'application/json'
                    }
                });
                const res = await fetch(request);
                if (res.status !== 200) {
                    setError(true);
                }
                const data = await res.json();
                setLoading(false);
                setCompany(data);
            }
        };
        f();
    }, [shouldRefetch]);

    const searchButtonClicked = (query) => {
        setShouldRefetch(!shouldRefetch);
        setQuery(query);
    }

    return (
        <div className={styles.container}>
            <Head>
                <title>{"Recherche d'entreprise"}</title>
            </Head>

            <main className={styles.main}>
                <div className={styles.mainContent}>
                    <h1 className={styles.title}>
                        Recherchez une entreprise!
                    </h1>
                    <SearchBarComponent searchButtonClicked={searchButtonClicked}/>

                    { loading ? <NoticeComponent>Chargeant...</NoticeComponent> : null }
                    { !loading && !error && company === null ? <NoticeComponent>{"Aucun résultat n'a été trouvé."}</NoticeComponent> : null }
                    { !loading && error ? <NoticeComponent>{"Il y avait une erreur en chargeant votre résultat."}</NoticeComponent> : null }
                    { company ? <CompanyInfoBoardComponent company={company}/> : null }

                </div>
            </main>

            <footer className={styles.footer}>
                <p>
                    Copyright: <a href="https://www.linkedin.com/in/florian-lackner1995/" target="_blank" rel="noreferrer">Florian Lackner</a> 2022
                </p>
            </footer>
        </div>
    )
}
